# BLZEpsonPrinterSDK

[![CI Status](https://img.shields.io/travis/neospirit/BLZEpsonPrinterSDK.svg?style=flat)](https://travis-ci.org/neospirit/BLZEpsonPrinterSDK)
[![Version](https://img.shields.io/cocoapods/v/BLZEpsonPrinterSDK.svg?style=flat)](https://cocoapods.org/pods/BLZEpsonPrinterSDK)
[![License](https://img.shields.io/cocoapods/l/BLZEpsonPrinterSDK.svg?style=flat)](https://cocoapods.org/pods/BLZEpsonPrinterSDK)
[![Platform](https://img.shields.io/cocoapods/p/BLZEpsonPrinterSDK.svg?style=flat)](https://cocoapods.org/pods/BLZEpsonPrinterSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
XCode 9.3.1
Cocoapods 1.5.2

## Installation

BLZEpsonPrinterSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BLZEpsonPrinterSDK'
```

## Author

neospirit, demare.cyrille@gmail.com

## License

BLZEpsonPrinterSDK is available under the MIT license. See the LICENSE file for more info.
