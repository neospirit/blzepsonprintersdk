#
# Be sure to run `pod lib lint BLZEpsonPrinterSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BLZEpsonPrinterSDK'
  s.version          = '2.11.0'
  s.summary          = 'Epson Printer Library as a pod'

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'The original Epson Printer iOS SDK can be found here: https://download.epson-biz.com/modules/pos/index.php?page=single_soft&cid=6077&scat=58&pcat=52'

  s.homepage         = 'https://neospirit@bitbucket.org/neospirit/blzepsonprintersdk.git'
  s.license          = { type: 'MIT', file: 'LICENSE' }
  s.author           = { 'neospirit' => 'demare.cyrille@gmail.com' }
  s.source           = { git: 'https://neospirit@bitbucket.org/neospirit/blzepsonprintersdk.git', tag: s.version.to_s }

  s.ios.deployment_target = '11.0'

  s.source_files = 'BLZEpsonPrinterSDK/Classes/**/*'

  # s.resource_bundles = {
  #   'BLZEpsonPrinterSDK' => ['BLZEpsonPrinterSDK/Assets/*.png']
  # }

  s.public_header_files = 'BLZEpsonPrinterSDK/Classes/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
